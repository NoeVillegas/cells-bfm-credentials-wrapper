{
  const {
    html,
  } = Polymer;
  /**
    `<cells-bfm-credentials-wrapper>` Description.

    Example:

    ```html
    <cells-bfm-credentials-wrapper></cells-bfm-credentials-wrapper>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-bfm-credentials-wrapper | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsBfmCredentialsWrapper extends Polymer.Element {

    static get is() {
      return 'cells-bfm-credentials-wrapper';
    }

    static get properties() {
      return {
        title: String,
        text: String,
        form: {
          type: Object,
          value: () => ({})
        },
        helperPrimary: {
          type: Object,
          value: () => ({})
        },
        helperSecondary: {
          type: Object,
          value: () => ({})
        }
      };
    }

  }

  customElements.define(CellsBfmCredentialsWrapper.is, CellsBfmCredentialsWrapper);
}